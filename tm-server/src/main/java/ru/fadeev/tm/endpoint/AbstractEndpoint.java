package ru.fadeev.tm.endpoint;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.service.IPropertyService;
import ru.fadeev.tm.api.service.ISessionService;
import ru.fadeev.tm.dto.SessionDTO;
import ru.fadeev.tm.util.EncryptUtil;

@Component
public class AbstractEndpoint {

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected ISessionService sessionService;

    protected String cryptSession(@Nullable final SessionDTO session) throws Exception {
        @NotNull final String key = propertyService.getSecretKey();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        return EncryptUtil.encrypt(json, key);
    }

    protected SessionDTO decryptSession(@Nullable final String cryptSession) throws Exception {
        @NotNull final String key = propertyService.getSecretKey();
        @NotNull final String json = EncryptUtil.decrypt(cryptSession, key);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final SessionDTO session = mapper.readValue(json, SessionDTO.class);
        return session;
    }

}