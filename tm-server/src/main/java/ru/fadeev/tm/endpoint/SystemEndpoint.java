package ru.fadeev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.ISystemEndpoint;
import ru.fadeev.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService(endpointInterface = "ru.fadeev.tm.api.endpoint.ISystemEndpoint")
public class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @WebMethod
    public String getHost(@WebParam(name = "token") @Nullable final String token) throws Exception {
        sessionService.checkSession(decryptSession(token), Role.ADMINISTRATOR);
        return propertyService.getServerHost();
    }

    @NotNull
    @WebMethod
    public String getPort(@WebParam(name = "token") @Nullable final String token) throws Exception {
        sessionService.checkSession(decryptSession(token), Role.ADMINISTRATOR);
        return propertyService.getServerPort();
    }

}