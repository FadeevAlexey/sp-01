package ru.fadeev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.IUserEndpoint;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.dto.SessionDTO;
import ru.fadeev.tm.dto.UserDTO;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.service.UserService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@Component
@WebService(endpointInterface = "ru.fadeev.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    @Autowired
    protected IUserService userService;

    @Override
    @WebMethod
    public List<UserDTO> findAllUser(@WebParam(name = "session") final String session) throws Exception {
        sessionService.checkSession(decryptSession(session), Role.ADMINISTRATOR);
        return userService.findAll()
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @Override
    @WebMethod
    public @Nullable UserDTO findOneUser(@WebParam(name = "session") final String token) throws Exception {
        @NotNull final SessionDTO session = decryptSession(token);
        sessionService.checkSession(session);
        @Nullable final User user = userService.findOne(session.getUserId());
        return convertToDTO(user);
    }

    @Override
    @WebMethod
    public @Nullable UserDTO removeUser(
            @WebParam(name = "token") final String token,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        sessionService.checkSession(decryptSession(token), Role.ADMINISTRATOR);
        userService.remove(id);
        return null;
    }

    @Override
    @WebMethod
    public void persistUser(@WebParam(name = "user") final @Nullable UserDTO user) throws Exception {
        userService.persist(convertToUser(user));
    }

    @Override
    @WebMethod
    public void mergeUserAdmin(
            @WebParam(name = "session") final String session,
            @WebParam(name = "user") final @Nullable UserDTO user
    ) throws Exception {
        sessionService.checkSession(decryptSession(session), Role.ADMINISTRATOR);
        userService.merge(convertToUser(user));
    }

    @Override
    @WebMethod
    public void mergeUser(
            @WebParam(name = "token") final String token,
            @WebParam(name = "user") final @Nullable UserDTO user
    ) throws Exception {
        @NotNull final SessionDTO session = decryptSession(token);
        if (session.getUserId().equals(user.getId()))
            userService.merge(convertToUser(user));
    }

    @Override
    @WebMethod
    public boolean isLoginExistUser(
            @WebParam(name = "login") @Nullable final String login) throws Exception {
        return userService.isLoginExist(login);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO findUserByLoginUser(
            @WebParam(name = "session") final String token,
            @WebParam(name = "login") @Nullable final String login
    ) throws Exception {
        sessionService.checkSession(decryptSession(token), Role.ADMINISTRATOR);
        return convertToDTO(userService.findUserByLogin(login));
    }

    @Nullable
    private User convertToUser(@Nullable final UserDTO userDTO) {
        if (userDTO == null) return null;
        @NotNull final User user = new User();
        user.setLogin(userDTO.getLogin());
        user.setPasswordHash(userDTO.getPasswordHash());
        user.setRole(userDTO.getRole());
        user.setId(userDTO.getId());
        return user;
    }

    @Nullable
    private UserDTO convertToDTO(@Nullable final User user) {
        if (user == null) return null;
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPasswordHash(user.getPasswordHash());
        userDTO.setRole(user.getRole());
        return userDTO;
    }

}