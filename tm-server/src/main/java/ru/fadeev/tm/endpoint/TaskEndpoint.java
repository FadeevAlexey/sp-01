package ru.fadeev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.ITaskEndpoint;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.dto.SessionDTO;
import ru.fadeev.tm.dto.TaskDTO;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.service.UserService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.stream.Collectors;

@Component
@WebService(endpointInterface = "ru.fadeev.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    protected IUserService userService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Override
    @Nullable
    @WebMethod
    public TaskDTO findOneTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        @Nullable final Task task = taskService.findOne(currentSession.getUserId(), taskId);
        return convertToDTO(task);
    }

    @Override
    @Nullable
    @WebMethod
    public TaskDTO removeTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        taskService.remove(currentSession.getUserId(), taskId);
        return null;
    }

    @Override
    @WebMethod
    public void persistTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "task") @Nullable final TaskDTO task
    ) throws Exception {
        if (task == null) return;
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        task.setUserId(currentSession.getUserId());
        taskService.persist(convertToTask(task));
    }

    @Override
    @WebMethod
    public void mergeTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "task") @NotNull final TaskDTO task
    ) throws Exception {
        @NotNull final SessionDTO session = sessionService.checkSession(decryptSession(token));
        if (session.getUserId().equals(task.getUserId()))
            taskService.merge(convertToTask(task));
    }

    @Override
    @Nullable
    @WebMethod
    public String findIdByNameTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        return taskService.findIdByName(currentSession.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDTO> findAllTask(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        return taskService.findAll(currentSession.getUserId())
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @Override
    @WebMethod
    public void removeAllTask(
            @WebParam(name = "token") final String token) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        taskService.removeAll(currentSession.getUserId());
    }

    @Override
    @NotNull
    @WebMethod
    public Collection<TaskDTO> searchByNameTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "string") @Nullable final String string
    ) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        return taskService.searchByName(currentSession.getUserId(), string)
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    @WebMethod
    public Collection<TaskDTO> searchByDescriptionTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "string") @Nullable final String string
    ) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        return taskService.searchByDescription(currentSession.getUserId(), string)
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    @WebMethod
    public Collection<TaskDTO> findAllByProjectIdTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        return taskService.findAllByProjectId(projectId, currentSession.getUserId())
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @Override
    @WebMethod
    public void removeAllByProjectIdTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        taskService.removeAllByProjectId(currentSession.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void removeAllProjectTask(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        taskService.removeAllProjectTask(currentSession.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDTO> sortByStartDateTask(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        return taskService.sortByStartDate(currentSession.getUserId())
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDTO> sortByFinishDateTask(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        return taskService.sortByFinishDate(currentSession.getUserId())
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDTO> sortByCreationTimeTask(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        return taskService.sortByCreationDate(currentSession.getUserId())
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDTO> sortByStatusTask(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final SessionDTO currentSession = sessionService.checkSession(decryptSession(token));
        return taskService.sortByStatus(currentSession.getUserId())
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @Nullable
    private Task convertToTask(@Nullable final TaskDTO taskDTO) throws Exception {
        if (taskDTO == null) return null;
        @NotNull final Task task = new Task();
        task.setId(taskDTO.getId());
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setProject(projectService.findOne(taskDTO.getProjectId()));
        task.setStartDate(taskDTO.getStartDate());
        task.setFinishDate(taskDTO.getFinishDate());
        task.setUser(userService.findOne(taskDTO.getUserId()));
        task.setStatus(taskDTO.getStatus());
        task.setCreationTime(taskDTO.getCreationTime());
        return task;
    }

    @Nullable
    private TaskDTO convertToDTO(@Nullable final Task task) {
        if (task == null) return null;
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        if (task.getProject() != null) taskDTO.setProjectId(task.getProject().getId());
        taskDTO.setStartDate(task.getStartDate());
        taskDTO.setFinishDate(task.getFinishDate());
        if (task.getUser() == null) return null;
        taskDTO.setUserId(task.getUser().getId());
        taskDTO.setStatus(task.getStatus());
        taskDTO.setCreationTime(task.getCreationTime());
        return taskDTO;
    }

}