package ru.fadeev.tm.repository;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.entity.Project;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ProjectRepository implements IProjectRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Nullable
    @Override
    public Project findOne(@NotNull String id) {
        return entityManager.find(Project.class, id);
    }

    @Override
    public void persist(@NotNull final Project project) {
        entityManager.persist(project);
    }

    @Override
    public void merge(@NotNull final Project project) {
        entityManager.merge(project);
    }


    @Override
    public void removeAll() {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<Project> cq = cb.createCriteriaDelete(Project.class);
        entityManager.createQuery(cq).executeUpdate();
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Project> cq = cb.createQuery(Project.class);
        @NotNull final Root<Project> rootEntry = cq.from(Project.class);
        @NotNull final CriteriaQuery<Project> all = cq.select(rootEntry);
        TypedQuery<Project> allQuery = entityManager.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public void remove(@NotNull final String id) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<Project> cq = cb.createCriteriaDelete(Project.class);
        @NotNull final Root<Project> rootEntry = cq.from(Project.class);
        cq.where(cb.equal(rootEntry.get("id"), id));
        entityManager.createQuery(cq).executeUpdate();
    }


    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<Project> cq = cb.createCriteriaDelete(Project.class);
        @NotNull final Root<Project> rootEntry = cq.from(Project.class);
        cq.where(cb.equal(rootEntry.get("user").get("id"), userId));
        entityManager.createQuery(cq).executeUpdate();
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId(@NotNull final String userId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Project> cq = cb.createQuery(Project.class);
        @NotNull final Root<Project> rootEntry = cq.from(Project.class);
        @NotNull final CriteriaQuery<Project> query = cq.select(rootEntry);
        cq.where(cb.equal(rootEntry.get("user").get("id"), userId));
        TypedQuery<Project> allQuery = entityManager.createQuery(query);
        return allQuery.getResultList();
    }

    @Override
    @Nullable
    public String findIdByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Project> cq = cb.createQuery(Project.class);
        @NotNull final Root<Project> rootEntry = cq.from(Project.class);
        @NotNull final CriteriaQuery<Project> query = cq.select(rootEntry);
        cq.where(
                cb.equal(rootEntry.get("user").get("id"), userId),
                cb.equal(rootEntry.get("name"), name)
        );
        TypedQuery<Project> allQuery = entityManager.createQuery(query);
        @Nullable final Project project = allQuery.getResultStream()
                .findFirst()
                .orElse(null);
        return project == null ? null : project.getId();
    }

    @NotNull
    @Override
    public Collection<Project> searchByName(@NotNull final String userId, @NotNull final String string) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Project> cq = cb.createQuery(Project.class);
        @NotNull final Root<Project> rootEntry = cq.from(Project.class);
        cq.where(
                cb.equal(rootEntry.get("user").get("id"), userId),
                cb.like(rootEntry.get("name"), "%" + string + "%")
        );
        TypedQuery<Project> allQuery = entityManager.createQuery(cq);
        return allQuery.getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> searchByDescription(@NotNull final String userId, @NotNull final String string) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Project> cq = cb.createQuery(Project.class);
        @NotNull final Root<Project> rootEntry = cq.from(Project.class);
        cq.where(
                cb.equal(rootEntry.get("user").get("id"), userId),
                cb.like(rootEntry.get("description"), "%" + string + "%")
        );
        TypedQuery<Project> allQuery = entityManager.createQuery(cq);
        return allQuery.getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> sortAllByStartDate(@NotNull final String userId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Project> cq = cb.createQuery(Project.class);
        @NotNull final Root<Project> rootEntry = cq.from(Project.class);
        cq.where(cb.equal(rootEntry.get("user").get("id"), userId));
        cq.orderBy(cb.asc(rootEntry.get("startDate")));
        TypedQuery<Project> allQuery = entityManager.createQuery(cq);
        return allQuery.getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> sortAllByFinishDate(@NotNull final String userId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Project> cq = cb.createQuery(Project.class);
        @NotNull final Root<Project> rootEntry = cq.from(Project.class);
        cq.where(cb.equal(rootEntry.get("user").get("id"), userId));
        cq.orderBy(cb.asc(rootEntry.get("finishDate")));
        TypedQuery<Project> allQuery = entityManager.createQuery(cq);
        return allQuery.getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> sortAllByStatus(@NotNull final String userId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Project> cq = cb.createQuery(Project.class);
        @NotNull final Root<Project> rootEntry = cq.from(Project.class);
        cq.where(cb.equal(rootEntry.get("user").get("id"), userId));
        cq.orderBy(cb.asc(rootEntry.get("status")));
        TypedQuery<Project> allQuery = entityManager.createQuery(cq);
        return allQuery.getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> sortAllByCreationDate(@NotNull final String userId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Project> cq = cb.createQuery(Project.class);
        @NotNull final Root<Project> rootEntry = cq.from(Project.class);
        cq.where(cb.equal(rootEntry.get("user").get("id"), userId));
        cq.orderBy(cb.asc(rootEntry.get("creationTime")));
        TypedQuery<Project> allQuery = entityManager.createQuery(cq);
        return allQuery.getResultList();
    }

    @Nullable
    @Override
    public Project findOneByUserId(@NotNull final String userId, @NotNull final String id) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Project> cq = cb.createQuery(Project.class);
        @NotNull final Root<Project> rootEntry = cq.from(Project.class);
        @NotNull final CriteriaQuery<Project> query = cq.select(rootEntry);
        cq.where(
                cb.equal(rootEntry.get("user").get("id"), userId),
                cb.equal(rootEntry.get("id"), id)
        );
        TypedQuery<Project> allQuery = entityManager.createQuery(query);
        return allQuery.getResultStream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<Project> cq = cb.createCriteriaDelete(Project.class);
        @NotNull final Root<Project> rootEntry = cq.from(Project.class);
        cq.where(
                cb.equal(rootEntry.get("user").get("id"), userId),
                cb.equal(rootEntry.get("id"), projectId)
        );
        entityManager.createQuery(cq).executeUpdate();
    }

}