package ru.fadeev.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import ru.fadeev.tm.context.Bootstrap;

@ComponentScan
public class Application {

    public static void main(String[] args) throws Exception {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(Application.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }

}