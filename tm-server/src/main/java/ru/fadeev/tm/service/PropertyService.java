package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

@Component
public class PropertyService implements IPropertyService {

    @NotNull
    private final Properties properties = new Properties();

    @Override
    public void init() throws Exception {
        @NotNull final String config = "/config.properties";
        final InputStream inputStream = PropertyService.class.getResourceAsStream(config);
        properties.load(inputStream);
    }

    @NotNull
    @Override
    public String getServerPort() {
        @Nullable final String systemServerPort = System.getProperty("server.port");
        if (systemServerPort != null && !systemServerPort.isEmpty()) return systemServerPort;
        @Nullable final String serverPort = properties.getProperty("server.port");
        if (serverPort == null) return "8080";
        return serverPort;
    }

    @NotNull
    @Override
    public String getServerHost() {
        @Nullable final String systemServerHost = System.getProperty("server.host");
        if (systemServerHost != null && !systemServerHost.isEmpty()) return systemServerHost;
        @Nullable final String serverHost = properties.getProperty("server.host");
        if (serverHost == null) return "localhost";
        return serverHost;
    }

    @Override
    @Nullable
    public String getSessionSalt() {
        @Nullable final String salt = properties.getProperty("server.port");
        if (salt == null) return "";
        return salt;
    }

    @Override
    public int getSessionCycle() {
        @Nullable final String cycle = properties.getProperty("session.cycle");
        if (cycle != null && cycle.matches("\\d+")) return Integer.parseInt(cycle);
        return 0;
    }

    @Override
    public int getSessionLifetime() {
        @Nullable final String cycle = properties.getProperty("session.lifetime");
        if (cycle != null && cycle.matches("\\d+")) return Integer.parseInt(cycle);
        return Integer.MAX_VALUE;
    }

    @NotNull
    public String getSecretKey() throws Exception {
        @Nullable final String secretKey = properties.getProperty("token.secretkey");
        if (secretKey == null || secretKey.isEmpty()) throw new Exception("Invalid config file");
        return secretKey;
    }

    @NotNull
    public String getBindAddress() throws Exception {
        @Nullable final String bindAddress = properties.getProperty("jms.bindaddress");
        if (bindAddress == null || bindAddress.isEmpty()) throw new Exception("Invalid config file");
        return bindAddress;
    }

}