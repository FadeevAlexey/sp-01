package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@Transactional
public class UserService extends AbstractService<User> implements IUserService {

    @Autowired
    IUserRepository userRepository;

    @Override
    public @NotNull List<User> findAll() {
        @NotNull final List<User> users = userRepository.findAll();
        return users;
    }

    @Override
    @Nullable
    public User findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @Nullable final User user = userRepository.findOne(id);
        return user;
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        userRepository.remove(id);
    }

    @Override
    public void removeAll() {
        userRepository.removeAll();
    }

    @Override
    public void persist(@Nullable final User user) {
        if (user == null) return;
        userRepository.persist(user);
    }

    @Override
    public void merge(@Nullable final User user) {
        if (user == null) return;
        userRepository.merge(user);
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        final boolean isLoginExist = userRepository.isLoginExist(login);
        return isLoginExist;
    }

    @Override
    @Nullable
    public User findUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        @Nullable final User user = userRepository.findUserByLogin(login);
        return user;
    }

    @Override
    public void setAdminRole(@Nullable final User user) {
        if (user == null) return;
        user.setRole(Role.ADMINISTRATOR);
        merge(user);
    }

    @NotNull
    private IUserRepository getUserRepository(@NotNull final EntityManager entityManager){
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext("ru.fadeev.tm");
        @NotNull final IUserRepository repository = context.getBean(IUserRepository.class);
        repository.setEntityManager(entityManager);
        return repository;
    }

}