package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.entity.Project;

import java.util.*;

@Service
@Transactional
public class ProjectService extends AbstractService<Project> implements IProjectService {

    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @Override
    public List<Project> findAll() {
        @NotNull final List<Project> projects = projectRepository.findAll();
        return projects;
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @Nullable final Project project = projectRepository.findOne(id);
        return project;
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String userId, final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @Nullable Project project = projectRepository.findOneByUserId(userId, id);
        return project;
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        projectRepository.remove(id);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        projectRepository.removeById(userId, projectId);
    }

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) return;
        projectRepository.persist(project);
    }

    @Override
    public void merge(@Nullable final Project project) {
        if (project == null) return;
        projectRepository.merge(project);
    }

    @Override
    public void removeAll() {
        projectRepository.removeAll();
    }

    @Nullable
    @Override
    public String findIdByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        @Nullable final String id = projectRepository.findIdByName(userId, name);
        return id;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        @NotNull final List<Project> projects = projectRepository.findAllByUserId(userId);
        return projects;
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.removeAllByUserId(userId);
    }

    @NotNull
    public Collection<Project> sortByStartDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Collection<Project> projects = projectRepository.sortAllByStartDate(userId);
        return projects;
    }

    @NotNull
    public Collection<Project> sortByFinishDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Collection<Project> projects = projectRepository.sortAllByFinishDate(userId);
        return projects;
    }

    @NotNull
    public Collection<Project> sortByStatus(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Collection<Project> projects = projectRepository.sortAllByStatus(userId);
        return projects;
    }

    @NotNull
    public Collection<Project> sortByCreationDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Collection<Project> projects = projectRepository.sortAllByCreationDate(userId);
        return projects;
    }

    @Override
    @NotNull
    public Collection<Project> searchByName(@Nullable final String userId, @Nullable final String string) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        @NotNull final Collection<Project> projects = projectRepository.searchByName(userId, string);
        return projects;
    }

    @Override
    @NotNull
    public Collection<Project> searchByDescription(@Nullable final String userId, @Nullable final String string) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        @NotNull final Collection<Project> projects = projectRepository.searchByDescription(userId, string);
        return projects;
    }

}