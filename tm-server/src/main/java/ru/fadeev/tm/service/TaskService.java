package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.fadeev.tm.api.repository.ITaskRepository;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.entity.Task;

import java.util.*;

@Service
@Transactional
public class TaskService extends AbstractService<Task> implements ITaskService {

    @Autowired
    private ITaskRepository repository;

    @Override
    public @NotNull List<Task> findAll() {
        @NotNull final List<Task> tasks = repository.findAll();
        return tasks;
    }

    @Override
    public void persist(@Nullable final Task task) {
        if (task == null) return;
        repository.persist(task);
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @Nullable final Task task = repository.findOne(id);
        return task;
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String userId, final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @Nullable final Task task = repository.findOneByUserId(userId, id);
        return task;
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        repository.remove(id);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        repository.removeByIdTask(userId, taskId);
    }

    @Override
    public void merge(@Nullable final Task task) {
        if (task == null) return;
        repository.merge(task);
    }

    @Override
    public void removeAll() {
        repository.removeAll();
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        repository.removeAllByUserId(userId);
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        @NotNull final Collection<Task> tasks = repository.findAllByUserId(userId);
        return tasks;
    }

    @Override
    @NotNull
    public Collection<Task> findAllByProjectId(@Nullable final String projectId, @Nullable final String userId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Collection<Task> tasks = repository.findAllByProjectId(projectId, userId);
        return tasks;
    }

    @Nullable
    @Override
    public String findIdByName(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        @Nullable final String id = repository.findIdByName(userId, name);
        return id;
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        repository.removeAllByProjectId(userId, projectId);
    }

    @Override
    public void removeAllProjectTask(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        repository.removeAllProjectTask(userId);
    }

    @NotNull
    @Override
    public Collection<Task> searchByName(@Nullable final String userId, @Nullable final String string) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        @NotNull final Collection<Task> tasks = repository.searchByName(userId, string);
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> searchByDescription(@Nullable final String userId, @Nullable final String string) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        @NotNull final Collection<Task> tasks = repository.searchByDescription(userId, string);
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortByStartDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Collection<Task> tasks = repository.sortAllByStartDate(userId);
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortByFinishDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Collection<Task> tasks = repository.sortAllByFinishDate(userId);
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortByStatus(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Collection<Task> tasks = repository.sortAllByStatus(userId);
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortByCreationDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Collection<Task> tasks = repository.sortAllByCreationDate(userId);
        return tasks;
    }

}