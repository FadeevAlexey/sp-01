package ru.fadeev.tm.context;

import lombok.Getter;
import lombok.Setter;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.*;
import ru.fadeev.tm.api.service.*;

import javax.xml.ws.Endpoint;

@Getter
@Setter
@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private ISessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    public void init() throws Exception {
        propertyService.init();
        startBrokerService();
        start();
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @Nullable final String serverHost = propertyService.getServerHost();
        @Nullable final String serverPort = propertyService.getServerPort();
        @NotNull final String link = String.format(
                "http://%s:%s/%s?wsdl", serverHost, serverPort, name);
        System.out.println(link);
        Endpoint.publish(link, endpoint);
    }

    public void start() {
        registry(userEndpoint);
        registry(taskEndpoint);
        registry(projectEndpoint);
        registry(sessionEndpoint);
        registry(systemEndpoint);
    }

    private void startBrokerService() {
        @NotNull final BrokerService broker = new BrokerService();
        try {
            broker.addConnector(propertyService.getBindAddress());
            broker.start();
        } catch (Exception ignored) {
        }
    }

}