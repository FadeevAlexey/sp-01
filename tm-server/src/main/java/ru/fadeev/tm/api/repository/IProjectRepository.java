package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Project;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public interface IProjectRepository {

    @NotNull
    List<Project> findAll();

    @Nullable
    Project findOne(@NotNull String id);

    void remove(@NotNull String id);

    void persist(Project project);

    void merge(@NotNull Project project);

    void removeAll();

    void removeAllByUserId(@NotNull String userId);

    @NotNull
    List<Project> findAllByUserId(@NotNull String userId);

    @Nullable
    String findIdByName(
            @NotNull String userId,
            @NotNull String name
    );

    @NotNull
    Collection<Project> searchByName(
            @NotNull String userId,
            @NotNull String string
    );

    @NotNull
    Collection<Project> searchByDescription(
            @NotNull String userId,
            @NotNull String string);

    @NotNull
    Collection<Project> sortAllByStartDate(@NotNull String userId);

    @NotNull
    Collection<Project> sortAllByFinishDate(@NotNull String userId);

    @NotNull
    Collection<Project> sortAllByStatus(@NotNull String userId);

    @NotNull
    Collection<Project> sortAllByCreationDate(@NotNull String userId);

    @Nullable
    Project findOneByUserId(
            @NotNull String userId,
            @NotNull String id
    );

    void removeById(
            @NotNull String userId,
            @NotNull String projectId);

    void setEntityManager(@NotNull EntityManager entityManager);

}
