package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IPropertyService {

    void init() throws Exception;

    @NotNull
    String getServerPort();

    @NotNull
    String getServerHost();

    @Nullable
    String getSessionSalt();

    int getSessionCycle();

    int getSessionLifetime();

    @NotNull
    String getSecretKey() throws Exception;

    @NotNull
    String getBindAddress() throws Exception;

}
