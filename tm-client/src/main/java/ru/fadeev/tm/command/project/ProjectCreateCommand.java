package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.IProjectEndpoint;
import ru.fadeev.tm.api.endpoint.ProjectDTO;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalProjectNameException;

@Component
public class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private IAppStateService appStateService;

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String token = appStateService.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        terminal.println("[PROJECT CREATE]");
        terminal.println("ENTER NAME:");
        @Nullable final String name = terminal.readString();
        if (name == null || name.isEmpty()) throw new IllegalProjectNameException("name can't be empty");
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        projectEndpoint.persistProject(token, project);
        terminal.println("[OK]\n");
        terminal.println("WOULD YOU LIKE EDIT PROPERTIES PROJECT ? USE COMMAND project-edit\n");
    }

}