package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.endpoint.ISessionEndpoint;
import ru.fadeev.tm.api.endpoint.IUserEndpoint;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.InvalidSessionException;

@Component
public final class UserLoginCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private ISessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    private IAppStateService appStateService;

    @NotNull
    @Override
    public String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "log in task manager";
    }

    @Override
    public void execute() throws Exception {
        terminal.println("[USER LOGIN]");
        terminal.println("ENTER LOGIN");
        @Nullable final String login = terminal.readString();
        @NotNull final boolean loginExist = userEndpoint.isLoginExistUser(login);
        if (!loginExist) throw new IllegalArgumentException("Can't find user");
        terminal.println("ENTER PASSWORD");
        @Nullable final String password = terminal.readString();
        if (password == null) throw new IllegalArgumentException("Illegal password");
        String token = sessionEndpoint.getToken(login, password);
        if (token == null || token.isEmpty()) throw new InvalidSessionException("Invalid token");
        appStateService.setToken(token);
        terminal.println("[OK]\n");
    }

}