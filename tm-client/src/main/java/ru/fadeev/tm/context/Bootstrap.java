package ru.fadeev.tm.context;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ru.fadeev.tm.api.repository.IAppStateRepository;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.CommandCorruptException;
import ru.fadeev.tm.exception.IllegalCommandNameException;

import java.lang.Exception;
import java.util.Map;

@Getter
@Setter
@Component
public class Bootstrap {

    @NotNull
    @Autowired
    private IAppStateRepository appStateRepository;

    @NotNull
    @Autowired
    private IAppStateService appStateService;

    @NotNull
    @Autowired
    private ITerminalService terminalService;

    public void init() {
        initCommand();
        start();
    }

    private void initCommand() {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext("ru.fadeev.tm");
        @NotNull final Map<String, AbstractCommand> commands = context.getBeansOfType(AbstractCommand.class);
        commands.values().forEach(this::registry);
    }

    private void start() {
        terminalService.println("*** WELCOME TO TASK MANAGER ***");
        @Nullable String command = "";
        while (!"exit".equals(command)) {
            try {
                command = getTerminalService().readString();
                execute(command);
            } catch (final Exception e) {
                terminalService.println(e.getMessage());
            }
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        @Nullable final String cliCommand = command.getName();
        @Nullable final String cliDescription = command.getDescription();
        if (cliCommand.isEmpty()) throw new CommandCorruptException();
        if (cliDescription.isEmpty()) throw new CommandCorruptException();
        appStateService.putCommand(cliCommand, command);
    }

    private void execute(@Nullable String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = appStateService.getCommand(command);
        if (abstractCommand == null) throw new IllegalCommandNameException("Wrong command name");
        abstractCommand.execute();
    }

}