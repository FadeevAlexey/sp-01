package ru.fadeev.tm.exception;

import org.jetbrains.annotations.Nullable;

public final class AccessDeniedException extends RuntimeException {

    public AccessDeniedException(@Nullable final String message) {
        super(message);
    }

}
