package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.fadeev.tm.api.endpoint.ProjectDTO;
import ru.fadeev.tm.api.endpoint.TaskDTO;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.constant.DateConst;
import ru.fadeev.tm.exception.IllegalDateException;
import ru.fadeev.tm.util.DateUtil;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Collection;
import java.util.Date;
import java.util.Scanner;

@Service
public final class TerminalService implements ITerminalService {

    @NotNull
    private final Scanner reader = new Scanner(System.in);

    @Nullable
    @Override
    public String readString() {
        return reader.nextLine();
    }

    @Nullable
    @Override
    public XMLGregorianCalendar readDate() throws Exception {
        @Nullable final String stringDate = readString();
        if (stringDate == null || stringDate.isEmpty())
            return null;
        if (stringDate.matches("^(0?[1-9]|[12][0-9]|3[01])[.](0?[1-9]|1[012])[.]\\d{4}$")) {
            Date date = DateConst.DATE_FORMAT.parse(stringDate);
            return DateUtil.calendarConverter(date);
        }
        throw new IllegalDateException("wrong format date");
    }

    @Override
    public void println(@Nullable final String string) {
        if (string == null)
            System.out.println("");
        System.out.println(string);
    }

    @Override
    public void print(@Nullable final String string) {
        if (string == null)
            System.out.print("");
        System.out.print(string);
    }

    @Override
    public void println(@Nullable final TaskDTO task) {
        if (task == null) {
            System.out.println("");
            return;
        }
        System.out.println(String.format(
                "Task(name: %-20.20s || status: %-10.10s " + "|| startDate:%-10.10s || finishDate:%-10.10s || description: %s)",
                task.getName(), task.getStatus(),
               DateUtil.dateToString(task.getStartDate()),
               DateUtil.dateToString(task.getFinishDate()), task.getDescription()));
    }

    @Override
    public void printTaskList(@Nullable final Collection<TaskDTO> tasks) {
        if (tasks == null) {
            System.out.println("");
            return;
        }
        int index = 1;
        for (@NotNull final TaskDTO task : tasks) {
            System.out.print(index + ". ");
            println(task);
        }
        System.out.println("");
    }

    @Override
    public void println(@Nullable final ProjectDTO project) {
        if (project == null) {
            System.out.println("");
            return;
        }
        System.out.println(String.format(
                "Project(name: %-20.20s || status: %-10.10s || startDate:%-10.10s || finishDate:%-10.10s || description: %s)",
                project.getName(), project.getStatus(),
                DateUtil.dateToString(project.getStartDate()),
                DateUtil.dateToString(project.getFinishDate()), project.getDescription()));
    }

    @Override
    public void printProjectList(@Nullable final Collection<ProjectDTO> projects) {
        if (projects == null) {
            System.out.println("");
            return;
        }
        int index = 1;
        for (@NotNull final ProjectDTO project : projects) {
            System.out.print(index + ". ");
            println(project);
        }
        System.out.println("");
    }

}